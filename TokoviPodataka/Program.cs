﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TokoviPodataka {
    class Program {
        static void Main(string[] args) {
            using (FileSystemWatcher watcher = new FileSystemWatcher()) {
                watcher.Path = @"D:\csharp\podaci";

                watcher.NotifyFilter = NotifyFilters.LastAccess
                                     | NotifyFilters.LastWrite
                                     | NotifyFilters.FileName
                                     | NotifyFilters.DirectoryName;

                watcher.Filter = "*.txt";
                watcher.Created += OnChanged;
                watcher.Changed += OnCreated;
                watcher.EnableRaisingEvents = true;

                while (Console.Read() != 'q');
            }

            #region komentari

            /* using (StreamWriter writer = new StreamWriter(@"D:\csharp\zapis_podaci.txt")) {
            }

            using (StreamReader sr = new StreamReader(@"D:\csharp\zapis_podaci.txt")) {
            } */


            /*List<string> podaci = new List<string>();

            using (StreamReader sr = File.OpenText(@"D:\csharp\zapisani_podaci.txt")) {
                string linija = null;
                sr.ReadLine();
                sr.ReadLine();
                while ((linija = sr.ReadLine()) != null) {
                    podaci.Add(linija);
                }
            }

            foreach (var podatak in podaci) {
                Console.WriteLine(podatak);
            }*/

            /*using (StreamWriter w = File.CreateText(@"D:\csharp\zapisani_podaci.txt")) {
                w.WriteLine("Podaci:");
                w.WriteLine("-------");
                foreach (var podatak in podaci) {
                    w.WriteLine(podatak);
                }
            }*/


            /*FileInfo zapis = new FileInfo(@"D:\csharp\zapis.txt");
            using (StreamWriter sw = zapis.CreateText()) {
                sw.WriteLine("Pozdrav");
                sw.WriteLine("iz");
                sw.WriteLine("C#");
            }*/


            /*string[] driveovi = Directory.GetLogicalDrives();
            Console.WriteLine("Driveovi:");
            foreach (string drive in driveovi) {
                Console.WriteLine(drive);
            }

            Console.WriteLine("Direktoriji");
            string[] direktoriji = Directory.GetDirectories(@"D:\csharp");
            foreach (string direktorij in direktoriji) {
                Console.WriteLine(direktorij);
            }*/

            /*DirectoryInfo direktorij = new DirectoryInfo(@"D:\csharp");

            direktorij.CreateSubdirectory("novi_dir");
            direktorij.CreateSubdirectory("novonovo");

            DirectoryInfo[] direktoriji = direktorij.GetDirectories();

            foreach (DirectoryInfo dir in direktoriji) {
                Console.WriteLine(dir.FullName);
            }*/
            /*FileInfo[] datoteke = direktorij.GetFiles("*.txt", SearchOption.AllDirectories);

            foreach (FileInfo f in datoteke) {
                Console.WriteLine("*****");
                Console.WriteLine("File name: {0}", f.Name);
                Console.WriteLine("File size: {0}", f.Length);
                Console.WriteLine("Creation: {0}", f.CreationTime);
                Console.WriteLine("Attributes: {0}", f.Attributes);
            }*/

            /*if (!direktorij.Exists) {
                direktorij.Create();
            }
            Console.WriteLine("Puno ime: {0}", direktorij.FullName);
            Console.WriteLine("Ime: {0}", direktorij.Name);
            Console.WriteLine("Roditelj: {0}", direktorij.Parent);
            Console.WriteLine("Izrađeno: {0}", direktorij.CreationTime);
            Console.WriteLine("Atributi: {0}", direktorij.Attributes);
            Console.WriteLine("Korijenski direktorij: {0}", direktorij.Root);*/
            #endregion komentari
        }

        private static void OnChanged(object source, FileSystemEventArgs e) {
            Console.WriteLine($"Datoteka: {e.FullPath} {e.ChangeType}");
        }
        private static void OnCreated(object source, FileSystemEventArgs e) {
            Console.WriteLine($"Datoteka: {e.FullPath} {e.ChangeType}");
        }
    }
}
