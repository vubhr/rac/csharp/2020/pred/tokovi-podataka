﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Serijalizacija {
    static class AlatiSerijalizacija {
        public static void SnimiUBinarnomFormatu(object objekt, string fileName) {
            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None)) {
                binFormat.Serialize(fs, objekt);
            }
        }

        public static object UcitajIzObjektnogFormata(string fileName) {
            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fStream = File.OpenRead(fileName)) {
                return binFormat.Deserialize(fStream);
            }
        }

        public static void SnimiUXml(object o, string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(o.GetType());
            using (Stream fs = new FileStream(fileName,
              FileMode.Create, FileAccess.Write, FileShare.None)) {
                xmlFormat.Serialize(fs, o);
            }
        }

        public static object UcitajIzXml(string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(typeof(Postavke));
            using (Stream fStream = File.OpenRead(fileName)) {
                return xmlFormat.Deserialize(fStream);
            }
        }

    }
}

