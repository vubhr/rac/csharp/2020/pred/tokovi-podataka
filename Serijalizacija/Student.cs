﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {

    [Serializable]
    class Student {
        public Student(int id, string ime, string prezime, DateTime datumRodjenja) {
            this.id = id;
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
        }
        public override string ToString() {
            return string.Format("{0} - {1} {2}, {3}", id, ime, prezime, datumRodjenja.ToString());
        }

        public int Id { get { return id; } }
        public string Ime { get { return ime; } }
        public string Prezime { get { return prezime; } }
        public DateTime DatumRodjenja { get { return datumRodjenja; } }

        [NonSerialized]
        public int id;
        public string ime;
        public string prezime;
        public DateTime datumRodjenja;
    }

}
