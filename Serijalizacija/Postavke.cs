﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
    [Serializable]
    public class Postavke {
        public Postavke() {
            zabranjeneLozinke = new List<string>();
        }
        public string verzija;
        public string direktorijSPodacima;
        public string korisnik;
        public List<string> zabranjeneLozinke;
    }
}

