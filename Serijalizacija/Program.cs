﻿using System;
using System.IO;

using System.Text.Json;
using System.Text.Json.Serialization;

namespace Serijalizacija {
    class Program {
        static void Main(string[] args) {
            #region postavke
            /*postavke = new Postavke();
            postavke.verzija = "1.0";
            postavke.direktorijSPodacima = @"D:\csharp\podaci";
            postavke.korisnik = "h00s";
            postavke.zabranjeneLozinke.Add("12345");
            postavke.zabranjeneLozinke.Add("password");
            postavke.zabranjeneLozinke.Add("112233");

            AlatiSerijalizacija.SnimiUXml(postavke, "postavke.xml");

            postavke = (Postavke)AlatiSerijalizacija.UcitajIzXml("postavke.xml");
            foreach (var lozinka in postavke.zabranjeneLozinke) {
                Console.WriteLine(lozinka);
            }*/

            #endregion postavke

            Student s = new Student(1337, "Marko", "Marulic", new DateTime(1450, 8, 18));

            string studentJson = JsonSerializer.Serialize(s);
            Console.WriteLine(studentJson);

            #region marulic2
            /*Student s = new Student(1337, "Marko", "Marulic", new DateTime(1450, 8, 18));
            Console.WriteLine(s);
            AlatiSerijalizacija.SnimiUBinarnomFormatu(s, "marulic.dat");

            Student s2 = new Student(2, "Drugo", "Ime", new DateTime());
            s2 = (Student)AlatiSerijalizacija.UcitajIzObjektnogFormata("marulic.dat");
            Console.WriteLine(s2);

            string student = JsonSerializer.Serialize(s, s.GetType());
            Console.WriteLine(student);*/
            #endregion marulic2

        }

        private  static void UcitajPostavke() {
            postavke = (Postavke)AlatiSerijalizacija.UcitajIzObjektnogFormata("postavke.dat");
        }

        private static void PohraniPostavke() {
            AlatiSerijalizacija.SnimiUBinarnomFormatu(postavke, "postavke.dat");
        }

        private static Postavke postavke;
    }
}

